all:
	echo "ut/hsc3-texts"

clean:
	echo "ut/hsc3-texts"

mk-cli:
	runhaskell mk/gen-cli.hs

push-gl:
	dir=ut r.gitlab-push.sh hsc3-texts
