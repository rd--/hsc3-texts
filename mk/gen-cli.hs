import Data.Char {- base -}
import Data.Function {- base -}
import Data.List {- base -}
import qualified Data.List.Split as Split {- split -}
import Data.Maybe {- base -}
import System.Process {- base -}
import Text.Printf {- base -}

import qualified Music.Theory.List as T {- hmt -}

prj_dir :: FilePath
prj_dir = "/home/rohan/ut/hsc3-texts/"

prj_file :: FilePath -> FilePath
prj_file = (++) prj_dir

sw_dir :: FilePath
sw_dir = "/home/rohan/sw/"

rem_sw_dir :: FilePath -> FilePath
rem_sw_dir = fromMaybe (error "rem_sw_dir") . stripPrefix sw_dir

-- > sep_str "/cmd/" "hsc3/cmd/scsynth.hs" == ("hsc3","scsynth.hs")
sep_str :: Eq a => [a] -> [a] -> ([a],[a])
sep_str x s =
    case Split.splitOn x s of
      [l,r] -> (l,r)
      _ -> error "sep_str"

-- | (section,(name,extension))
type CLI = (String,(String,String))

-- > sep "hsc3/cmd/scsynth.hs" == ("hsc3",("scsynth","hs"))
sep :: String -> CLI
sep nm =
    let (prj,fn) = sep_str "/cmd/" nm
    in (prj,sep_str "." fn)

is_mod :: CLI -> Bool
is_mod cli =
    case cli of
      (_,(c:_,_)) -> isUpper c
      _ -> error "is_mod"

is_excluded :: CLI -> Bool
is_excluded cli =
    let (sec,(nm,_)) = cli
    in sec `elem` ["pandoc-minus","hcamera","hnutr"] ||
       nm `elem` ["cgi"] ||
       is_mod cli

gen_list_f :: CLI -> String
gen_list_f (sec,(nm,x)) =
    let fmt = "- %s [[?](?t=%s&e=md/%s.md),[%s](?t=%s&e=cmd/%s.%s)]"
    in printf fmt nm sec nm x sec nm x

gen_cs_f :: CLI -> String
gen_cs_f (sec,(nm,_)) =
    let fmt = "[%s](?t=%s&e=md/%s.md),"
    in printf fmt nm sec nm

t2_cons :: t -> u -> (t,u)
t2_cons p q = (p,q)

type CLI_SET = (String,[(String,String)])

gen_md_sec :: CLI_SET -> [String]
gen_md_sec (sec,cmd) =
    let hdr = "# " ++ sec
        readme = printf "- [README](?t=%s)" sec
        mn = "" : hdr : "" : readme : map (gen_list_f . t2_cons sec) cmd
        rm = "" : map (gen_cs_f . t2_cons sec) cmd
    in mn ++ rm

gen_cli :: [FilePath] -> [CLI_SET]
gen_cli =
    T.collate .
    sortBy (compare `on` (\(_,(nm,_)) -> nm)) .
    filter (not . is_excluded) .
    map (sep . rem_sw_dir)

main :: IO ()
main = do
  cli_hs <- readProcess "sh" ["-c","ls ~/sw/*/cmd/*.hs"] ""
  cli_c <- readProcess "sh" ["-c","ls ~/sw/*/cmd/*.c"] ""
  cli_cpp <- readProcess "sh" ["-c","ls ~/sw/*/cmd/*.cpp"] ""
  let cli = gen_cli (lines cli_hs ++ lines cli_c ++ lines cli_cpp)
      cli_md = unlines (concatMap gen_md_sec cli)
  writeFile (prj_file "8.1-cli.md") cli_md
