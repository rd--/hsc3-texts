hsc3-texts
----------

[Haskell](http://haskell.org/)
[SuperCollider](http://audiosynth.com/)
[Bindings](http://rohandrape.net/t/hsc3)

- 1.1 [Introduction](?t=hsc3-texts&e=1.1-introduction.md) (February, 2008)
- 2.1 [Tutorial](?t=hsc3-texts&e=2.1-tutorial.md) (January, 2007)
  + 2.2 [Drawing](?t=hsc3-texts&e=2.2-drawing.md)
  + 2.3 [Plotting](?t=hsc3-texts&e=2.3-plotting.md)
- 3.1 [Graph Representation](?t=hsc3-texts&e=3.1-graph-representation.md) (July, 2012)
- 3.2 [Graph Rewriting](?t=hsc3-texts&e=3.2-graph-rewriting.md) (July, 2012)
- 7.1 [Patterns Introduction](?t=hsc3-texts&e=7.1-patterns-introduction.md) (February, 2008)
  + 7.2 [Patterns Further](?t=hsc3-texts&e=7.2-patterns-further.md)
- 8.1 [Command Line Interfaces](?t=hsc3-texts&e=8.1-cli.md) (November, 2015)

<!-- - [Event Model](?t=hsc3-texts&e=lhs/event-model.lhs) (February, 2008) -->

Libraries:

- [hsc3](http://rohandrape.net/t/hsc3)
- [hsc3-dot](http://rohandrape.net/t/hsc3-dot)
- [hsc3-plot](http://rohandrape.net/t/hsc3-plot)
- [hsc3-lang](http://rohandrape.net/t/hsc3-lang)

© [rohan drape](http://rohandrape.net/),
  2006-2024,
  [gpl](http://gnu.org/copyleft/).
